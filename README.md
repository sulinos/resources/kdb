# KDb

KDb is a database connectivity and creation framework, consisted of a general-purpose
C++ Qt library and set of plugins delivering support for various database vendors.

It is a part of the [Kexi project](http://www.calligra.org/kexi) and the general
[Calligra Suite](http://www.calligra.org).

Project's home page: http://community.kde.org/KDb

Project maintainer: Jarosław Staniek <staniek@kde.org>


# Building KDb

KDb uses the CMake build system. Up-to-date information is available
[online](http://community.kde.org/KDb/Build).
